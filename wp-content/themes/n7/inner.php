<section class="project-details-outer">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="about-project-inner">
<div class="ab-project text">
<h2>Bellevue Towers</h2>
<p>We are delighted to offer for rent this fantastic three bedroom (Type 7) independent villa to the market in Saheel, Arabian Ranches. The property has been very well maintained and comes with a private pool.</p>
</div>
<div class="developer-details text">
<h3>Dubai developers</h3>
<ul>
<li><span><img src="<?php echo get_template_directory_uri(); ?>/images/home-area.gif"></span>AED 1,867,000</li>
<li><span><img src="<?php echo get_template_directory_uri(); ?>/images/area2.png"></span>AREA: 4,794 SQFT</li>
</ul>
</div>
<div class="developer-details text">
<h3>Amenities</h3>
<ul>
<li><span><img src="<?php echo get_template_directory_uri(); ?>/images/bedroom.png"></span>2,3 Bedroom Apartment</li>
<li><span><img src="<?php echo get_template_directory_uri(); ?>/images/bathroom.png"></span>2,3 Bathroom</li>
</ul>
<ul class="without-icon">
<li>Private pool</li>
<li>Type 7 villa</li>
<li>Single row</li>
<li>Quiet location</li>
</ul>
</div>
<div class="explore-dealer"><a href="javascript:;">Possesion: Q4 - 2019</a></div>
</div>
</div>
<div class="col-md-6">
<div class="project-inner-gallery">

<div class="home-latest-outer">
          <div id="big-inner" class="owl-carousel owl-theme">
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>

          </div>
          <div id="thumbs-inner" class="owl-carousel owl-theme">
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>
            <div class="item">
              <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
            </div>

          </div>
        </div>


</div>
</div>
</div>
</div>
</section>
<!--end project-details-outer-->


<section class="paymentplan">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="inner-section-heading text">
<h2>Payment Plan</h2></div>
</div>


<div class="col-md-6">
<div class="paymentplaninner">
<h5>Down Payment – 5% </h5>
<p>On Booking</p>
</div>
</div>
<div class="col-md-6">
<div class="paymentplaninner">
<h5>1st Installment – 5% </h5>
<p>Within 30 Days of Booking</p>
</div>
</div>
<div class="col-md-6">
<div class="paymentplaninner">
<h5>Down Payment – 5% </h5>
<p>On Booking</p>
</div>
</div>
<div class="col-md-6">
<div class="paymentplaninner">
<h5>1st Installment – 5% </h5>
<p>Within 30 Days of Booking</p>
</div>
</div>
<div class="col-md-6">
<div class="paymentplaninner">
<h5>Down Payment – 5% </h5>
<p>On Booking</p>
</div>
</div>
<div class="col-md-6">
<div class="paymentplaninner">
<h5>1st Installment – 5% </h5>
<p>Within 30 Days of Booking</p>
</div>
</div>
</div>
</div>
</section>
<!--end paymentplan-->


<section class="broker-inquery">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="inner-section-heading text">
<h2>Register your Interest</h2></div>
</div>


<div class="broker-form-inner">
<form>

<div class="fl-left">
<div class="form-field">
<input type="name" name="name" placeholder="Name">
</div>
<div class="form-field">
<input type="text" name="tel" placeholder="Telephone Number">
</div>
<div class="form-field">
<input type="email" name="email" placeholder="Email">
</div>
</div>
<div class="fr-right">
<div class="form-field">
<textarea placeholder="Your Message Here"></textarea>
</div>
</div>
<div class="clearfix"></div>
<div class="cs-check">
 <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
    <label class="custom-control-label" for="customCheck">I agree to the Terms and Conditions, Privacy Policy & Cookie Policy.</label>
  </div>
</div>
<div class="cs-check">
 <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="customCheck1" name="example2">
    <label class="custom-control-label" for="customCheck1">I agree to receiving regular newsletters in accordance with the privacy policy.</label>
  </div>
 </div> 
 
 <div class="submit">
 <button type="submit">Submit</button>
 
 </div>

</form>
</div>
</div>
</div>
</section>



<section class="dealer-contact">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="dealer-project-map">
<img src="<?php echo get_template_directory_uri(); ?>/images/map.jpg">
</div>
</div>
<div class="col-md-6">
<div class="dealer-contact-buttons">
<h2>Contact US</h2>

<div class="call-dealer d_req">
<a href="javascript:;">Call + 971 12 345 678</a>
</div>
<div class="whatsaap-dealer d_req">
<a href="javascript:;">Chat On WhatsApp</a>
</div>
</div>
</div>
</div>
</div>
</section>



<section class="related-project">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="inner-section-heading text">
<h2>Similar Properties you might like</h2></div>

<div class="rel-project">
<div class="owl-carousel owl-theme">

<div class="item">
<div class="related-project-inner">
<div class="rel-images"><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/related-project.jpg"></a></div>
<div class="rel-description">
<h4><a href="javascript:;">Serena Casa Viva-Dora</a></h4>
<ul>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bead-room.png"></span>
<span class="rel-des">2,3 Bedroom Apartment</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bath-room.png"></span>
<span class="rel-des">2,3 Bathroom</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/area.png"></span>
<span class="rel-des">AREA: 4,794 SQFT</span></li>
</ul>
</div>
</div>
</div>


<div class="item">
<div class="related-project-inner">
<div class="rel-images"><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/related-project.jpg"></a></div>
<div class="rel-description">
<h4><a href="javascript:;">Serena Casa Viva-Dora</a></h4>
<ul>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bead-room.png"></span>
<span class="rel-des">2,3 Bedroom Apartment</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bath-room.png"></span>
<span class="rel-des">2,3 Bathroom</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/area.png"></span>
<span class="rel-des">AREA: 4,794 SQFT</span></li>
</ul>
</div>
</div>
</div>


<div class="item">
<div class="related-project-inner">
<div class="rel-images"><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/related-project.jpg"></a></div>
<div class="rel-description">
<h4><a href="javascript:;">Serena Casa Viva-Dora</a></h4>
<ul>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bead-room.png"></span>
<span class="rel-des">2,3 Bedroom Apartment</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bath-room.png"></span>
<span class="rel-des">2,3 Bathroom</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/area.png"></span>
<span class="rel-des">AREA: 4,794 SQFT</span></li>
</ul>
</div>
</div>
</div>


<div class="item">
<div class="related-project-inner">
<div class="rel-images"><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/related-project.jpg"></a></div>
<div class="rel-description">
<h4><a href="javascript:;">Serena Casa Viva-Dora</a></h4>
<ul>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bead-room.png"></span>
<span class="rel-des">2,3 Bedroom Apartment</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bath-room.png"></span>
<span class="rel-des">2,3 Bathroom</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/area.png"></span>
<span class="rel-des">AREA: 4,794 SQFT</span></li>
</ul>
</div>
</div>
</div>



<div class="item">
<div class="related-project-inner">
<div class="rel-images"><a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/related-project.jpg"></a></div>
<div class="rel-description">
<h4><a href="javascript:;">Serena Casa Viva-Dora</a></h4>
<ul>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bead-room.png"></span>
<span class="rel-des">2,3 Bedroom Apartment</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bath-room.png"></span>
<span class="rel-des">2,3 Bathroom</span></li>
<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/area.png"></span>
<span class="rel-des">AREA: 4,794 SQFT</span></li>
</ul>
</div>
</div>
</div>

</div>
</div>


</div>
</div>
</div>
</section>
<!--end related-project-->







