<style>
#home-banner{display:none;}
</style>
<section class="banner-innerslider banner-inner" id="inner-banner">
  <header>
    <div class="container-fluid">
      <div class="trigger">
        <i class="fa fa-bars" aria-hidden="true"></i>
      </div>
      <div class="logo"><a href="<?php echo get_site_url(); ?>">
        <picture>
          
          <?php
          $innerlogoimage = get_field('logo-inner', 'option');
          if( !empty($innerlogoimage) ): ?>
          <source media="(max-width: 850px)" srcset="<?php echo get_site_url(); ?>/wp-content/uploads/2019/06/logo.png">
          <img src="<?php echo $innerlogoimage['url']; ?>" alt="<?php echo $innerlogoimage['alt']; ?>" />
          <?php endif; ?>
        </picture>
      </a></div>
      <div class="navigation">
        <ul>
          <li><span><i class="fa fa-phone" aria-hidden="true"></i></span> <span class="letstalk">Let’s talk</span> <a href="tel:+971 123455678">+971 48746767</a></li>
          <li><span><i class="fa fa-envelope" aria-hidden="true"></i></span><a href="javasvcript:;" data-toggle="modal" data-target="#myModal">Register your interest</a></li>
        </ul>
      </div>
      <div class="nav768">
        <ul>
          <li><a href="tel:+971 123455678"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
          <li><a href="javascript:;" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
        </ul>
      </div>
      <div class="clearfix"></div>
    </div>
  </header>
  
  <section class="innerbanner-slider">
    <div class="owl-carousel owl-theme">
      
      <?php if(is_tax()){
      //echo $termid = get_queried_object()->term_id;
      if( have_rows('inner_banner_slider',get_queried_object()) ){ ?>
      <?php while( have_rows('inner_banner_slider',get_queried_object()) ): the_row();
      $imagenew = get_sub_field('inner_banner_images');
      $contentnew = get_sub_field('inner_banner_caption');
      $no_apartment = get_sub_field('no-of-apartment');
      $readmore = get_sub_field('banner-read-more-link');
      ?>
      <div class="item">
      <figure><img src="<?php echo $imagenew['url']; ?>" alt="<?php echo $imagenew['alt'] ?>">
	   <div class="banner-caption">
        <h3> <?php echo $contentnew; ?></h3>
        <?php if($no_apartment){?> <h4><?php echo $no_apartment; ?></h4>
        
        <?php } if($readmore){ ?>
        
        <div class="read_more_banner"><a href="<?php echo $readmore; ?>">Read More</a></div>
        <?php } ?>
      </div>
	  </figure>
      <div class="bc_hover"></div>
     
    </div>
    <?php endwhile; ?>
    
    
    
    <?php }  else{ ?>
    <div class="item">
    <figure><img src="<?php echo get_template_directory_uri(); ?>/images/dummy-banner.jpg" alt="<?php echo $imagenew['alt'] ?>">
	<div class="banner-caption">
      <h3> <?php echo $contentnew; ?></h3>
    </div>
	</figure>
    <div class="bc_hover"></div>
    
  </div>
  <?php } ?>
  
  <?php }else if( have_rows('inner_banner_slider',get_the_ID()) ){ ?>
  
  
  <?php $i=0; while( have_rows('inner_banner_slider') ): the_row();
  
  $image = get_sub_field('inner_banner_images');
  $content = get_sub_field('inner_banner_caption');
  
  $i++;
  ?>
  <div class="item">
  <figure><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
  <div class="banner-caption">
    <h3> <?php echo $content; ?></h3>
  </div>
  </figure>
  <div class="bc_hover"></div>
  
</div>
<?php endwhile; if($i==0){ ?>
<div class="item">
<figure><img src="<?php echo get_template_directory_uri(); ?>/images/dummy-banner.jpg" alt="<?php echo $image['alt'] ?>">
<div class="banner-caption">
  
</div>
</figure>
<div class="bc_hover"></div>

</div>
<?php } ?>
<?php } else if(is_404()){?>
<div class="item">
<figure><img src="<?php echo get_template_directory_uri(); ?>/images/404-error.jpg" alt=""></figure>
<div class="bc_hover"></div>
</div>
<?php } else{ ?>
<div class="item">
<figure><img src="<?php echo get_template_directory_uri(); ?>/images/dummy-banner.jpg" alt="<?php echo $image['alt'] ?>">
<div class="banner-caption">
</div>
</figure>
<div class="bc_hover"></div>

</div>
<?php } ?>
</div>
</section>
</section>
<!--end banner-->