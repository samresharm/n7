<?php
if(empty($post_meta['latitude'][0]) && empty($post_meta['longitude'][0])){ ?>
<section class="dealer-contact">
  <div class="container">
    <div class="dealer-contact-buttons">
      <h2>Contact Us</h2>
      <div class="d-flex justify-content-center flex-wrap">
        <div class="call-dealer d_req">
          <a href="tel:+97148746767">Call + 971 487 467 67</a>
        </div>
        <div class="whatsaap-dealer d_req">
          <a href="https://wa.me/97148746767" target="_blank">Chat On WhatsApp</a>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } else{ ?>



<section class="dealer-contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 order-md-1 order-lg-0">
                <div class="dealer-project-map">
                    <iframe width="100%" height="300" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $post_meta['latitude'][0]; ?>,<?php echo $post_meta['longitude'][0]; ?>&amp;key=AIzaSyCZmX3oa7KEnTADxneD3GF_jsW4le8vAn0"></iframe>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 order-md-0 order-lg-1">
                <div class="dealer-contact-buttons">
                    <h2 style="text-align:left;">Contact US</h2>

                    <div class="call-dealer d_req">
                        <a href="tel:+97148746767">Call + 971 487 467 67</a>
                    </div>
                    <div class="whatsaap-dealer d_req">
                        <a href="https://wa.me/97148746767" target="_blank">Chat On WhatsApp</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>