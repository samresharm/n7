<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "site-content" div.
*
* @package WordPress
* @subpackage Twenty_Sixteen
* @since Twenty Sixteen 1.0
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
        <?php endif; ?>
        <?php wp_head(); ?>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fonts.css" type="text/css" media="all" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/viewer.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styles.css" type="text/css" media="all" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" type="text/css" media="all" />
		

    </head>
    <body <?php body_class(); ?>>
        <?php if ( is_tax('project_type') || is_single() || is_page_template() || is_tax('project_property_tag') || is_404() ) {?>
        <?php include('inner-header.php');?>
        <?php } else { ?>
        <section class="banner" style="background: url(<?php echo get_template_directory_uri(); ?>/images/banner1.jpg) no-repeat top center; background-size:cover !important;">
            <header>
                <div class="container-fluid">
                    <div class="trigger">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </div>
                    <div class="logo"><a href="<?php echo get_site_url(); ?>">
					<?php
					$logoimage = get_field('logo', 'option');
					if( !empty($logoimage) ): ?>
						<img src="<?php echo $logoimage['url']; ?>" alt="<?php echo $logoimage['alt']; ?>" />
					<?php endif; ?>
					</a></div>
                    <div class="navigation">
                        <ul>
                            <li><span><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <span class="letstalk">Let’s talk</span><a href="tel:+971 123455678">+971 48746767</a></li>
                            <li><span><i class="fa fa-envelope" aria-hidden="true"></i></span><a href="javascript:;" data-toggle="modal" data-target="#myModal">Register your interest</a></li>
                        </ul>
                    </div>
                    <div class="nav768">
                        <ul>
                            <li><a href="tel:+971 123455678"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:;" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </header>
            <?php include ('search-property.php'); ?>
        </section>
        <!--end banner-->
        <?php } ?>
        <div class="overlay"></div>
        <section class="mega_menu">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'main-menu',
					'container_class' => 'custom-menu-class',
						'menu_id'         => 'top-menu1',) );
				?>

        </section>





		<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content col-xl-12">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">REGISTER YOUR INTEREST</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

	  <?php wp_reset_query(); ?>
<div class="broker-form-inner">
  <form id="contact-header" method="post">
    <div class="row">
    <div class="col-md-6">
      <div class="form-field">
        <input type="name" name="fname" placeholder="Name" data-validation="custom" data-validation-regexp="^([a-zA-Z]+)$">
      </div>
      <div class="form-field">
        <div class="mobile-field">
          <div class="select">
            <div class="select-style">
              <select name="tel" class="dialer">
                <option>+971</option>
                <option>+972</option>
                <option>+973</option>
                <option>+974</option>
              </select>
            </div>
          </div>
            <input type="hidden" name="page_id" value="<?php echo get_the_title(); ?>"/>
          <input type="text" name="phone" placeholder="Telephone Number" maxlength="10" data-validation="custom" data-validation-regexp="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$" />
        </div>
      </div>
      <div class="form-field">
        <input type="email" name="email" placeholder="Email" data-validation="email" class="email">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-field">
        <textarea placeholder="Your Message Here" name="msg"></textarea>
      </div>
    </div>
    </div>
    <div class="cs-check">
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="customCheck" name="example1" data-validation="required" data-validation-error-msg="You have to agree to our terms">
        <label class="custom-control-label" for="customCheck">I agree to the Terms and Conditions, Privacy Policy & Cookie Policy.</label>
      </div>
    </div>
    <div class="cs-check">
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="customCheck1" name="example2" data-validation="required" data-validation-error-msg="You have to agree to our terms">
        <label class="custom-control-label" for="customCheck1">I agree to receiving regular newsletters in accordance with the Privacy Policy.</label>
      </div>
    </div>

    <div class="submit">
      <button type="submit">Submit</button>
        <div id="thank-header"></div>
    </div>

  </form>
</div>


      </div>


    </div>
  </div>
</div>
<style>
  .custom-checkbox span.form-error{
    margin-left: -25px;
  }
</style>
