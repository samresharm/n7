<?php
/*
* Template Name: sell
*/
get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="project-sell">
	<div class="container">
		<div class="rent-inner-banner">
			<img src="<?php echo get_template_directory_uri(); ?>/images/sell-left-banner.jpg">
		</div>
		<div class="project-sell-left">
			<div class="rent-inner-content">
				<h2>Selling made<br/>
				convenient</h2>
				<p>Want to sell your property but don’t want to go through the hassle of having to manage endless processes and talking to numerous sources? We’ve got the perfect solution for you – just list your property here are we’ll take care of everything for you! </p>
				<!-- <div class="sell-inner-banner-left">
					<img src="<?php echo get_template_directory_uri(); ?>/images/sell-right-banner.jpg">
				</div> -->
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section class="project-sell">
	<div class="container">
		<div class="rent-inner-banner">
			<img src="<?php echo get_template_directory_uri(); ?>/images/sell-right-banner.jpg">
		</div>
		<div class="project-sell-left">
			<div class="rent-inner-content">
				<h2>What makes Us Different?</h2>
				<p>Our team of experienced professionals will see to it that you experience little to no problems in listing your property and getting it sold at the drop of a hat. So, say goodbye to the headache of having to sell your property – N7 has all your bases covered!</p>
				<!-- <div class="sell-inner-banner-left">
					<img src="<?php echo get_template_directory_uri(); ?>/images/sell-right-banner.jpg">
				</div> -->
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section class="broker-inquery">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner-section-heading text">
				<h2>Register your Interest</h2></div>
			</div>
			<?php include('inc-broker-contact-form.php')?>
		</div>
	</div>
</section>
<!-- <section class="rent-contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="dealer-contact-buttons">
					<h2>Contact US</h2>
					<div class="call-dealer d_req">
						<a href="javascript:;">Call + 971 487 467 67</a>
					</div>
					<div class="whatsaap-dealer d_req">
						<a href="javascript:;">Chat On WhatsApp</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<?php include('inc-dealer-contact.php');?>
<?php endwhile;?>
<?php endif;?>
<?php get_footer(); ?>
</body>
</html>