<?php
/**
* The template for displaying the footer
*
* Contains the closing of the #content div and all content after
*
* @package WordPress
* @subpackage Twenty_Sixteen
* @since Twenty Sixteen 1.0
*/
?>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="footer-link">
          <h4>HELP AND SUPPORT</h4>
          <ul>
            <li><a href="<?php echo get_site_url();?>/contact-us">Contact Us</a></li>
            <li><a href="<?php echo get_site_url();?>/career">Careers</a></li>
            <li><a href="<?php echo get_site_url();?>/Privacy-Policy">Privacy Policy</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3">
        <div class="footer-link">
          <h4>ABOUT N7</h4>
          <ul>
            <li><a href="javascript:;">Investor Relations</a></li>
            <li><a href="javascript:;">Press Releases</a></li>
          </ul>
        </div>
      </div>
      <div class="offset-md-1 col-md-5">
        <div class="subscrib">
          <h4>SIGN UP FOR EMAIL ALERTS</h4>
          <form id="newsletter" method="post">
            <div class="width100">
              <div class="width80"><input type="email" name="newsletteremail" data-validation="email" placeholder="Enter your email adress"></div>
              <div class="width20"><button type="submit">Sign up</button>
			  </div>
              <div class="clearfix"></div>
			   <p id="thank-newsletter"></p>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 order-sm-1 order-1 order-md-0 order-lg-0">
        <div class="copy-right"> © N7 Real Estate . All Rights Reserved</div>
      </div>
      <div class="offset-md-1 col-md-5 order-sm-0 order-0 order-md-1 order-lg-1">
        <div class="follow-us">
          <h4>Follow us on</h4>
          <ul>
            <li><a href="https://www.facebook.com/n7realestate/" target="_blank"><i class="fa fa-facebook" aria-hidden="true" target="_blank"></i></a></li>
            <li><a href="https://twitter.com/n7realestate?s=08" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/n7realestate/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="https://www.linkedin.com/company/n7-real-estate/about/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<!--script src="<?php echo get_template_directory_uri(); ?>/js/equal-height.js"></script-->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-multiselect.js"></script>
<script>
   


$(document).ready(function() {
  $('#developerselect').multiselect({
    buttonWidth : '100px',
	nonSelectedText: 'Developer'
  });
   $('#property-typeselect').multiselect({
    buttonWidth : '100px',
	nonSelectedText: 'Property type'
  });
   $('#bedroomselect').multiselect({
    buttonWidth : '100px',
	nonSelectedText: 'Bedroom'
  });
   $('#bathroomselect').multiselect({
    buttonWidth : '100px',
	nonSelectedText: 'Bathroom'
  });
   $('#locationselect').multiselect({
    buttonWidth : '100px',
	nonSelectedText: 'Location'
  });
});


</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.form-validator.js"></script>
<script>


$(function() {
// setup validate
$.validate();
});
siturl ='<?php echo site_url(); ?>';
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/viewer.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/thumbs.js"></script>
<script>
$(function(){
  $('.search-menu ul li').click(function(){
    $('.search-menu ul li').removeClass('active');
    $(this).addClass('active');
  })
  $(".if-no-content").each(function(){
if($(this).children().length == 0){
$(this).hide();
} else{
$(this).show();
}
});
});
$('#contact').submit(function(e){
mydata = $('#contact').serialize();
url ='<?php echo admin_url( 'admin-ajax.php' ) ?>'+'?action=contact';
//console.log($('#contact').isValid());
e.preventDefault();
if($('#contact').isValid()){
console.log(url);
mydata = $('#contact').serialize();
$.ajax({
type: 'post',
url: url,
data: mydata,
success: function(res) {
if(res)
{
$('#contact')[0].reset();
$('#thank').html('<p> Thank you for showing your Interest');
  }
  else{
  $('#thank').html('<p>Please check your input field before submit the form</p>');
  }
  }
  });
  }
  })
  
  
  
  $('#contact-header').submit(function(e){
mydata = $('#contact-header').serialize();
url ='<?php echo admin_url( 'admin-ajax.php' ) ?>'+'?action=contact';
//console.log($('#contact-header').isValid());
e.preventDefault();
if($('#contact-header').isValid()){
console.log(url);
mydata = $('#contact-header').serialize();
$.ajax({
type: 'post',
url: url,
data: mydata,
success: function(res) {
if(res)
{
$('#contact-header')[0].reset();
$('#thank-header').html('<p> Thank you for showing your Interest');
  }
  else{
  $('#thank-header').html('<p>Please check your input field before submit the form</p>');
  }
  }
  });
  }
  })
  
  
   $('#newsletter').submit(function(e){
mydata = $('#newsletter').serialize();
url ='<?php echo admin_url( 'admin-ajax.php' ) ?>'+'?action=newsletter';
//console.log($('#contact-header').isValid());
e.preventDefault();
if($('#newsletter').isValid()){
console.log(url);
mydata = $('#newsletter').serialize();
$.ajax({
type: 'post',
url: url,
data: mydata,
success: function(res) {
if(res)
{
$('#newsletter')[0].reset();
$('#thank-newsletter').html('<p> Thank you for showing your Interest');
  }
  else{
  $('#thank-newsletter').html('<p>Please check your input field before submit the form</p>');
  }
  }
  });
  }
  })
  
  
  </script>
  <script>
  $(function () {
  $("#off-plan .c_show").slice(0, 5).show();
  $("#loadMore").on('click', function (e) {
  e.preventDefault();
  $("#off-plan .c_show:hidden").slice(0, 5).slideDown();
  if ($("#off-plan .c_show:hidden").length == 0) {
  $(".laodmore-btn").fadeOut('slow');
  }
  
  });
  });
  </script>
  <script>
  $(function() {
  var people = [];
  $.getJSON('<?php echo get_template_directory_uri(); ?>/images/dilar-code.json', function(data) {
  $.each(data.code, function(i, f) {
  var tblRow = "<option value=" + f.dial_code + ">" + f.dial_code + "</option>";
      console.log(tblRow);
  $(tblRow).appendTo(".dialer");
  });
  });
  });
  </script>
  
  
  <script>
  $(function(){
$('.home-project-list-new .owl-carousel').owlCarousel({
    loop:false,
    margin:0,
    responsiveClass:true,
	autoplayHoverPause:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:true
        },
        1000:{
            items:4,
            nav:false,
            loop:false
        }
    }
})
})
  </script>
  <?php


  ?>
<script type='text/javascript'>
  //$("#developerselect option[value='47']").attr("selected", 1);
  //$("#developerselect option[value='47']").prop("selected", true);
 //$("#developerselect").multiselect("refresh");
    //$('#developer select').val([47, 48]);
</script>
  
  
  
</body>
</html>