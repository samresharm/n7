<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package WordPress
* @subpackage Twenty_Sixteen
* @since Twenty Sixteen 1.0
*/
get_header(); ?>
<section class="about-us" id="aboutus">
  <div class="container-fluid">
    <div class="home-aboutimg text-align-right"><img src="<?php echo get_template_directory_uri(); ?>/images/about-banner.jpg" class="img-responsive"></div>
    <div class="text homeabout-text">
      <h2>About Us</h2>
      <p>We are a real estate company based in the heart of Dubai, providing a host of solutions to satisfy consumer needs. Our team of passionate industry experts perform extensive research and analysis to provide the best services possible to our clients and ensure the one thing that everyone requires at the end of the day – satisfaction.</p>
      <p> We take care of all the end-to-end processes involved in buying, selling, renting, and asset management, among other such requirements. This is accomplished by working in tandem with our customers, which helps us identify, analyse, and satisfy any and all of their requirements pertaining to the real estate industry.</p>
      <p>With Dubai being one of the most desirable and sought-after places to stay in the world, we aim to leave our customers with a long-lasting smile on their face as they make their dreams a reality. Our real estate offerings aim to accentuate one's lifestyle in this hub of grandeur and affluence, and we assure you that our services will go a long way in assuring a one-of-a-kind experience.</p>
      <div class="learn-more tg-mob"><a href="javascript:;">Read More</a></div
    </div>
  </div>
  <div class="clearfix"></div>
</section>
<?php $terms = get_terms( 'project_property_tag' );
echo '<section class="home-latest-project home-project-list-new"> <div  class="owl-carousel owl-theme">';
  foreach ( $terms as $term ) {
  $images = get_field('project-property-banner', $term);
  echo '<div class="item"><div class="home_poroject"><img src="'.$images['url'].'"><div class="home_project_name">
    <a href="'.get_term_link($term->slug, 'project_property_tag').'"><span>'. $term->name . '</span></a></div></div></div>';
    }
  echo '</div></section>';
  ?>
  <section class="home-latest-project-view docs-pictures">
    <div class="container">
      <div class="section-heading">Latest Projects</div>
      <div class="lt_slider latest-slider-home" id="latest-slider-home">
        <div  class="owl-carousel owl-theme">
          <div class="item">
            <div class="row">
              <div class="col-md-12 col-lg-6">
                <div class="home-latest-outer">
                  <video width="100%"  controls id="vdo" poster="<?php echo get_template_directory_uri(); ?>/images/poster.jpg" >
                    <source src="<?php echo get_template_directory_uri(); ?>/images/jbr.mp4" type="video/mp4">
                  </video>
                  <div class="home-slider-video thumbnail-img">
                    <div  class="docs-pictures123">
                      <div class="item">
                        <img data-original="<?php echo site_url(); ?>/wp-content/uploads/2019/05/dubai-pro-jbr-big1.jpg" src="<?php echo site_url(); ?>/wp-content/uploads/2019/05/dubai-pro-jbr-big1.jpg">
                      </div>
                      <div class="item">
                        <img data-original="<?php echo site_url(); ?>/wp-content/uploads/2019/05/dubai-pro-jbr-big2.jpg" src="<?php echo site_url(); ?>/wp-content/uploads/2019/05/dubai-pro-jbr-big2.jpg">
                      </div>
                      <div class="item">
                        <img data-original="<?php echo site_url(); ?>/wp-content/uploads/2019/05/dubai-pro-jbr-big3.jpg" src="<?php echo site_url(); ?>/wp-content/uploads/2019/05/dubai-pro-jbr-big3.jpg">
                      </div>
                      <div class="item">
                        <img data-original="<?php echo site_url(); ?>/wp-content/uploads/2019/05/dubai-pro-jbr-big4.jpg" src="<?php echo site_url(); ?>/wp-content/uploads/2019/05/dubai-pro-jbr-big4.jpg">
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-lg-6">
                <div class="home-project-video">
                  <ul class="tabs">
                    <li class="tab-link" data-tab="tab-1">Dubai Properties - JBR</li>
                  </ul>
                  <div id="tab-1" class="tab-content current">
                    <p>An uber residential enclave of 40 high-profile towers, Jumeirah Beach Residence is also known for al fresco dining and shopping, making it among the most sought-after addresses in Dubai. Thoughtfully designed and meticulously planned, the 2 to 4-bedroom apartments and 5-bedroom penthouses at 1JBR pair contemporary design with exquisite interiors and spectacular. The apartments feature living rooms with panoramic vistas, designer fittings, spacious bedrooms with en-suite bathrooms and wide sundecks. With an eye on the future, the units at 1JBR will feature smart home technology so residents can customise their own home as well.</p>
                    <div class="learn-more"><a href="<?php echo get_site_url(); ?>/projects/jbr/">Learn More</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-md-12 col-lg-6">
                <div class="home-latest-outer">
                  <div class="">
                    <img data-original="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery1.jpg" src="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery1.jpg">
                  </div>
                  <div class="home-slider-video thumbnail-img">
                    <div  class="docs-pictures123">
                      <div class="item">
                        <img data-original="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery1.jpg" src="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery1.jpg">
                      </div>
                      <div class="item">
                        <img data-original="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery2.jpg" src="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery2.jpg">
                      </div>
                      <div class="item">
                        <img data-original="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery3.jpg" src="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery3.jpg">
                      </div>
                      <div class="item">
                        <img data-original="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery4.jpg" src="<?php echo get_template_directory_uri(); ?>/images/emmar-home-gallery4.jpg">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-lg-6">
                <div class="home-project-video">
                  <ul class="tabs">
                    <li class="tab-link" data-tab="tab-1">Emaar - ROVE</li>
                  </ul>
                  <div id="tab-1" class="tab-content current">
                    <p>Rove City Walk is the premier hotel room investment product with incredible returns of 8%. The hotel is situated in City Walk, a popular family-friendly neighbourhood with a sophisticated ambiance and a unique mix of residences, high-end retail, dining, entertainment, hospitality, grooming and wellness options. This property serves as one of the best and most lucrative investments in the heart of Dubai.</p>
                    <div class="learn-more"><a href="<?php echo get_site_url(); ?>/projects/rove/">Learn More</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--end home-latest-project-view-->
  <section class="home-blog">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-heading">From Our Blog</div>
        </div>
        <div class="col-md-12">
          <div id="home-blog">
            <div class="owl-carousel owl-theme">
              <div class="item">
                <div class="blog-post">
                  <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2019/06/n7-blog3.jpg">
                  <div class="blog-listing">
                    <h3><a href="javascript:;">Sell your property with ease</a></h3>
                    <p>5 things you should always keep in mind while selling your property in Dubai</p>
                    <div class="learn-post-more"><a href="javascript:;">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="blog-post">
                  <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2019/06/n7-blog2.jpg">
                  <div class="blog-listing">
                    <h3><a href="javascript:;">Your real estate investment guide </a></h3>
                    <p>Dubai: A goldmine of near-unlimited opportunities</p>
                    <div class="learn-post-more"><a href="javascript:;">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="blog-post">
                  <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2019/06/n7-blog1.jpg">
                  <div class="blog-listing">
                    <h3><a href="javascript:;">Real estate facts you didn't know!</a></h3>
                    <p>Read on to know amazing facts about real estate in Dubai</p>
                    <div class="learn-post-more"><a href="javascript:;">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--end home-blog-->
  <section class="map">
    <iframe width="100%" height="300" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Westburry Tower 1, Business Bay, Dubai, UAE&amp;key=AIzaSyCZmX3oa7KEnTADxneD3GF_jsW4le8vAn0"></iframe>
  </section>
  <!--end map-->
  <?php get_footer(); ?>