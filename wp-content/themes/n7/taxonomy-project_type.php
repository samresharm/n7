<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;



get_header(); ?>


<?php if ( have_posts() ) : ?>


<?php include('inner-search.php');?>

<section class="pagination-sec">
    <div class="container">
      <div class="d-flex justify-content-end flex-wrap">
        <div class="view-div">
          <ul>
            <li class="list-view" id="list-view"><a href="javascript:;"><i class="fa fa-list-ul" aria-hidden="true"></i></a></li>
            <li class="list-view" id="grid-view"><a href="javascript:;"><i class="fa fa-th" aria-hidden="true"></i></a></li>
          </ul>
        </div>
        <div class="pagination-div col-sm-12 col-12">
         <?php echo wpbeginner_numeric_posts_nav() ;?>
        </div>
        <div class="perpage-view col-sm-12 col-12">
          <div class="select">
          <form name="myform" action="" method="post">
            <select name='posts_per_page' onchange="myform.submit();">
              <option value='10' > 10 per page </option>
              <option value='20'> 20 per page </option>
              <option value='30'> 30 per page </option>
              <option value='40'> 40 per page </option>
              <option value='50'> 50 per page </option>
            </select>
        </form>
          </div>
        </div>
      </div>
    </div>
  </section>



  <section class="buy-grid">
    <div class="container" id="remove-class">
      <div class="buyproductlist">

			<?php
			// Start the Loop.
            $k =0;
			while ( have_posts() ) :
				the_post();
                    $post_meta = get_post_meta(get_the_ID());
                    $developer =get_the_terms(get_the_ID(),'builder');
					$location =get_the_terms(get_the_ID(),'location');
					$property_tag =get_the_terms(get_the_ID(),'property_tag');
					$bathroom =get_the_terms(get_the_ID(),'bathroom'); 


                    $ourdeveloper = '';
                    foreach($developer as $key =>$value){
                    $ourdeveloper.= $value->name. ',';

                    }
					$bathroomkey ='';
					foreach($bathroom as $key =>$value){
                    $bathroomkey.= $value->name. ',';

                    }
					
					$property_tag_key ='';
					foreach($property_tag as $key =>$value){
                    $property_tag_key.= str_replace('BR','',$value->name). ',';

                    }
					
                    $ourdeveloper = substr($ourdeveloper ,0,-1);
                    $pricevariation = '';
                    $gallery_images = '';
                    $gallery_thumb ='';
                    for ($i = 0 ;$i <$post_meta['gallery_images'][0]; $i++)
                    {
						if($i>0) break;
                        $thumbimage = wp_get_attachment_image_src($post_meta['gallery_images_'.$i.'_gallery_big_image'][0],'full size');
                        $bigimage = wp_get_attachment_image_src($post_meta['gallery_images_'.$i.'_gallery_big_image'][0],'full size');

                        $gallerydescription =$post_meta['gallery_images_'.$i.'_image_description'][0];

                        $gallery_images .='<figure><img src="'.$bigimage[0].'" alt="'.$gallerydescription.'"></figure>';

                    }

                   $no_of_room ='';
$no_of_toilet ='';
    for ($i = 0 ;$i <$post_meta['price_variation'][0]; $i++)
    {
       $no_of_room = $post_meta['price_variation_'.$i.'_no_of_room'][0].',';
       $no_of_toilet = $post_meta['price_variation_'.$i.'_no_of_bathroom'][0].',';
    }


            ?>

					 <div class="buy-product">
          <div class="grid-inner">
            <div class="project-grid-image s-main">
              <div class="project-img s-main1">
                                
                                  <?php if($gallery_images){ echo $gallery_images; }
								   else{ ?>
								   <figure><a href="<?php the_permalink(); ?>"><img data-original="<?php echo get_template_directory_uri(); ?>/images/gallery-dummy.jpg" src="<?php echo get_template_directory_uri(); ?>/images/gallery-dummy.jpg" alt=""></a></figure>
								   <?php } ?>
                               
                            </div>
               <a href="<?php the_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" class="width_100" ></a>
              <div class="project-grid-details withouthover">
                <h4><?php echo get_the_title(); ?></h4>
                <ul>
                 <?php if (!empty($post_meta['area'][0])){ ?>
                                            <li><span><img src="<?php echo get_template_directory_uri(); ?>/images/area2.png"></span>AREA: <?php echo $post_meta['area'][0]; ?> SQFT</li>
                                     <?php }  ?>

									
                  <?php if(!empty($property_tag_key)){?>
                  <li><span><img src="<?php echo get_template_directory_uri(); ?>/images/bead-room-grid.png"></span><?php $property_tag_key;?> BED</li>
					<?php } ?>

                </ul>
              </div>
              <div class="project-grid-details on-hover s-main2">
			  <div class="p-table-inner">
			  <div class="p-tablecell-inner">
			  <?php if (!empty($post_meta['standard_price'][0])){ ?>
                                           <div class="proj-price"><img src="<?php echo get_template_directory_uri(); ?>/images/home-icon.png"> &nbsp; AED <?php echo number_format($post_meta['standard_price'][0]); ?></div>
                                       <?php } else{ ?>
									   <div class="proj-price"><img src="<?php echo get_template_directory_uri(); ?>/images/home-icon.png"> &nbsp; Price on request </div>
									<?php } ?>
                <h4><?php echo get_the_title(); ?></h4>
                <div class="gr-details">
                  <p><?php echo $ourdeveloper; ?></p>
                  <span><?php echo get_the_excerpt(); ?></span>
                  <div class="v_more"><a href="<?php the_permalink(); ?>">View More</a></div>
                </div>
                <ul>

				 <?php if (!empty($post_meta['area'][0])){ ?>
                                             <li><span><img src="<?php echo get_template_directory_uri(); ?>/images/area-grid.png"></span>AREA: <?php echo $post_meta['area'][0]; ?> SQFT</li>
                                       <?php } ?>

					<?php if(!empty($property_tag_key)){?>
                  <li><span><img src="<?php echo get_template_directory_uri(); ?>/images/bead-room-grid.png"></span><?php $property_tag_key;?> BED</li>
					<?php } ?>
				  
				  
                </ul>
              </div>
			  </div>
			  </div>
			  <div class="clearfix"></div>
            </div>
          </div>
        </div>


      <?php
			endwhile;

			// Previous/next page navigation.

?>

		</div>
		</div>
		</section>


		<section class="property-made">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <div class="pr-made">
            <p>
              PROPERTY BUYING<br>
              MADE EASY
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="broker-inquery">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="inner-section-heading text">
<h2>Register your Interest</h2></div>
</div>

  <?php include('inc-broker-contact-form.php');?>

</div>
</div>
</section>



  <?php include('inc-dealer-contact.php');?>

 <section class="related-project">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="inner-section-heading text">
<h2>Similar Properties you might like</h2></div>

<div class="rel-project">
    <div class="owl-carousel owl-theme">
   <?php $args = array('post_type'=>'projects',

            'tax_query' => array(

                    array(
                    'taxonomy' => 'builder',
                    'field' => 'id',
                    'terms' => '46'
                    ),
                    array(
                    'taxonomy' => 'project_type',
                    'field' => 'id',
                    'terms' => '43'
                    ),

                )
             );
        $query=new WP_Query($args);
        while ($query->have_posts() ) :
			$query->the_post();

        $no_of_room ='';
        $no_of_toilet ='';
        for ($i = 0 ;$i <$post_meta['price_variation'][0]; $i++)
        {
            $no_of_room = $post_meta['price_variation_'.$i.'_no_of_room'][0].',';
            $no_of_toilet = $post_meta['price_variation_'.$i.'_no_of_bathroom'][0].',';
        }
        ?>


        <div class="item">
        <div class="related-project-inner">
        <div class="rel-images"><a href="<?php echo get_the_permalink(); ?>"> <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" ></a></div>
        <div class="rel-description">
        <h4><a href="<?php echo get_permalink();?>"><?php echo get_the_title(); ?></a></h4>
        <ul>
             <?php if ($no_of_room){ ?>
                    <li class="rel-icon"><span><img src="<?php echo get_template_directory_uri(); ?>/images/bedroom.png"> </span><?php echo $no_of_room; ?> Bedroom Apartment</li>
                    <?php } ?>
                    <?php if ($no_of_toilet){ ?>
                    <li class="rel-icon"><span><img src="<?php echo get_template_directory_uri(); ?>/images/bathroom.png"> </span><?php echo $no_of_toilet; ?>  Bathroom</li>
                    <?php } ?>
                    <?php if ($post_meta['area'][0]){ ?>
                        <li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/area2.png"> </span>AREA: <?php echo $post_meta['area'][0]; ?> SQFT</li>
                        <?php } ?>


        </ul>
        </div>
        </div>
        </div>



    <?php
	endwhile;
?>

</div>
</div>

</div>
</div>
</div>
</section>


<?php
			// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>


<?php get_footer(); ?>
