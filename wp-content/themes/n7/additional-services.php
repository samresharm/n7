<?php
/*
* Template Name: additional-services
*/
get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="project-sell">
	<div class="container">
		<div class="rent-inner-banner">
			<img src="<?php echo get_template_directory_uri(); ?>/images/additional-services-1.jpg">
		</div>
		<div class="project-sell-left">
			<div class="rent-inner-content">
				<h2>Property Management</h2>
				<p><i>Supervise and manage your properties</i></p>
				<p>It can be quite a headache to leave a property you own for days, months or even years on end. Thankfully, with our property management services, you will never have to worry about this situation ever again. Take a gander at everything we have to offer in this regard!</p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section class="project-sell">
	<div class="container">
		<div class="rent-inner-banner">
			<img src="<?php echo get_template_directory_uri(); ?>/images/additional-services-2.jpg">
		</div>
		<div class="project-sell-left">
			<div class="rent-inner-content">
				<h2>lease</h2>
				<p><i>Live in the lap of luxury</i></p>
				<p>Reside in the hotspots of Dubai at reasonable rates! We’ll help you at every step of the renting process. From connecting with landlords to sorting out your documentation – we assure you that there are no headaches that you’ll experience in the leasing process.</p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section class="project-sell">
	<div class="container">
		<div class="rent-inner-banner">
			<img src="<?php echo get_template_directory_uri(); ?>/images/additional-services-3.jpg">
		</div>
		<div class="project-sell-left">
			<div class="rent-inner-content">
				<h2>Facility management</h2>
				<p><i>Supervise and manage your properties</i></p>
				<p>It can be quite a headache to leave a property you own for days, months or even years on end. Thankfully, with our property management services, you will never have to worry about this situation ever again. Take a gander at everything we have to offer in this regard!</p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section class="broker-inquery broker-inquery-bar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner-section-heading text">
				<h2>Register your Interest</h2></div>
			</div>
			<?php include('inc-broker-contact-form.php')?>
		</div>
	</div>
</section>
<!-- <section class="rent-contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="dealer-contact-buttons">
					<h2>Contact US</h2>
					<div class="call-dealer d_req">
						<a href="javascript:;">Call + 971 487 467 67</a>
					</div>
					<div class="whatsaap-dealer d_req">
						<a href="javascript:;">Chat On WhatsApp</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<?php include('inc-dealer-contact.php');?>
<?php endwhile;?>
<?php endif;?>
<?php get_footer(); ?>
</body>
</html>