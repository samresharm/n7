<?php wp_reset_query(); ?>
<div class="broker-form-inner">
  <form id="contact" method="post">
    <div class="row">
    <div class="col-md-6">
      <div class="form-field">
        <input type="name" name="fname" placeholder="Name" data-validation="custom" data-validation-regexp="^([a-zA-Z]+)$">
      </div>
      <div class="form-field">
        <div class="mobile-field">
          <div class="select">
            <div class="select-style">
              <select name="tel"  class="dialer">
              </select>
            </div>
          </div>
            <input type="hidden" name="page_id" value="<?php echo get_the_title(); ?>"/>
          <input type="text" name="phone" placeholder="Telephone Number" maxlength="10" data-validation="custom" data-validation-regexp="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$" data-validation-help="Please enter valid mobile no.">
        </div>
      </div>
      <div class="form-field">
        <input type="email" name="email" placeholder="Email" data-validation="email" class="email">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-field">
        <textarea placeholder="Your Message Here" name="msg"></textarea>
      </div>
    </div>
    </div>
    <div class="cs-check">
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="customCheck3" name="example1" data-validation="required" data-validation-error-msg="You have to agree to our terms">
        <label class="custom-control-label" for="customCheck3">I agree to the Terms and Conditions, Privacy Policy & Cookie Policy.</label>
      </div>
    </div>
    <div class="cs-check">
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="customCheck4" name="example2" data-validation="required" data-validation-error-msg="You have to agree to our terms">
        <label class="custom-control-label" for="customCheck4">I agree to receiving regular newsletters in accordance with the Privacy Policy.</label>
      </div>
    </div>

    <div class="submit">
      <button type="submit">Submit</button>
        <div id="thank"></div>
    </div>

  </form>
</div>
