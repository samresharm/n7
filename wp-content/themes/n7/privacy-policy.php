<?php
/*
* Template Name: privacy policy
*/
get_header(); ?>


<section class="privacy-policy-cust">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center text">
				<h3>Privacy Policy</h3>
				<p>Listed below are the details of the Privacy Policy, which dictates how N7 Group collects, uses, and maintains information collected from users on the website. This policy applies to all the offerings on this site, and using this site means that you agree with this Privacy Policy.</p>
				<div class="priv-sec">
					<p class="bold-text"><strong>Collection of information</strong></p>
					<p>In order to contact you and provide our goods & services, we will need to collect information about you. This information is collected in numerous ways, including – but not limited to – when users visit our site, avail our services, fill out a form, and perform other such activities. This comes in the form of personally identifiable information, such as your name, address, email address, and phone number.</p>
				</div>
				<div class="priv-sec">
					<p class="bold-text"><strong>Usage of Information</strong></p>
					<p>We may use your personal information to market third-party offerings to you. However, rest assured - we will always obtain your permission prior to providing your details to the aforementioned third parties. You will also always have the option to avoid being contacted for these products and services.</p>
				</div>
				<div class="priv-sec">
					<p class="bold-text"><strong>Usage of Information</strong></p>
					<p>We may use your personal information to market third-party offerings to you. However, rest assured - we will always obtain your permission prior to providing your details to the aforementioned third parties. You will also always have the option to avoid being contacted for these products and services.</p>
				</div>
				<div class="priv-sec">
					<p class="bold-text"><strong>Cookies Policy</strong></p>
					<p>Our site uses cookies to strive and enhance the overall user experience. For the uninitiated, a cookie refers to a small amount of data that often includes a unique identifier, sent to the device's hard drive through which you're accessing the browser. Cookies record relevant information pertaining to your online preferences and also allows us to tailor the website as per your interests. Users may choose to have their browser alert them when cookies are being sent, or refuse cookies altogether. However, keep in mind that choosing the latter might result in the website not working properly at times.</p>
					<p>N7 will never engage in the sale, rent, or barter of your personal information for any reason. Any changes to this privacy policy is at our discretion, so we highly recommend users to visit this page at regular intervals.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="privacy-policy-cust term-and-conditions">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center text">
				<h3>Terms and Conditions</h3>
				<p>By using our website – and,  by extension, our services – you hereby agree and adhere to all the terms and conditions listed below.</p>
				<div class="priv-sec">
					<p class="bold-text"><strong>Copyright</strong></p>
					<p>All the content and design of this website belongs to N7 Group and is protected by copyright, trademarks, and other intellectual property rights. You can download/print property details, images, and anything of the sort for personal non-commercial use only. You may not alter any of the images or details and make them available for public viewing or use them commercially.</p>
				</div>
				<div class="priv-sec">
					<p class="bold-text"><strong>Warranty exclusion</strong></p>
					<p>We at N7 strive to provide the best and most accurate information possible. However, at the same time, we must clarify that we don't provide a warranty of any kind regarding accuracy, completeness, currency, or reliability of the materials and information provided on this website.</p>
				</div>
				<div class="priv-sec">
					<p class="bold-text"><strong>Liability</strong></p>
					<p>N7 is not responsible for any damage that might be caused due to your actions – or lack thereof – as a result of viewing or reading any of the content or information displayed on this website. Keep in mind that all of the information featured on this website should be taken as general guidance only. You are responsible to gauge the accuracy of said information before going forward with any costs or expenses.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
</body>
</html>