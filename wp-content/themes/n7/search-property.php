<div class="container">
  <div class="row m-0">
    <form>
    <div class="search">
      <div class="search-menu">
		
<?php 
$terms = get_terms( array('taxonomy' => 'project_type','orderby' => 'name','order' => 'DESC' ) );
if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
    echo '<ul>';
    foreach ( $terms as $term ) {
        $checked ='';
        $active ='';
        if(isset($_REQUEST['active-property']) && ($_REQUEST['active-property']==$term->term_id) )
        {
            $checked ='checked';
            $active = 'active';
        }else if(!isset($_REQUEST['active-property']) && ($term->term_id ==43))
		{
			$checked ='checked';
            $active = 'active';
		}
        echo '<li class="'.$active.'"><input type="radio" name="active-property" value="'.$term->term_id.'" '.$checked.' >' . $term->name . '</li>';
    }
    echo '<li><a href="http://13.233.16.157/n7/sell/">Sell</a></li></ul>';
}
global $wpdb;
$property_type_tarm = get_terms( 'project_property_tag' );  
 $property_tag =  get_terms( 'property_tag' );
 //$sql ="select distinct(meta_value) as city from ".$wpdb->prefix."postmeta where meta_key= 'city' and meta_value !=''";
//$result =$wpdb->get_results($sql);
 $location =  get_terms( array('taxonomy' => 'location' ) );       
?>
  		
      </div>

      <div class="serach-pannel home-filter">
      
          <div id="property-type" class="property-filter">
            <select name="property[]" multiple class="form-control" id="property-typeselect">
              <?php foreach($property_type_tarm as $key =>$value){ ?>
                     <option value="<?php echo $value->term_id; ?>"
                             <?php if(isset($_GET['property']) && in_array($value->term_id ,$_GET['property'])) {  echo 'selected="selected" checked'; } ?>><?php echo $value->name; ?></option>
            <?php  } ?>
            </select>
          </div>
          <div id="budget" class="property-filter">
            <select name="minbudget" id="budgetselect"  class="form-control">
			<option value="0">Min Price</option>
              <option value="200000"  <?php if(isset($_REQUEST['minbudget']) && ( 200000 ==$_REQUEST['minbudget'])) { echo 'selected;'; } ?>>AED 200,000</option>
              <option value="AED 12000000" <?php if(isset($_REQUEST['minbudget']) && ( 12000000 ==$_REQUEST['minbudget'])) { echo 'selected;'; }?>>AED 12,000,000</option>
              <option value="22000000" <?php if(isset($_REQUEST['minbudget']) && ( 22000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 22,000,000</option>
			   <option value="32000000" <?php if(isset($_REQUEST['minbudget']) && ( 32000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 32,000,000</option>
			   <option value="42000000" <?php if(isset($_REQUEST['minbudget']) && ( 42000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 42,000,000 </option>
			   <option value="52000000" <?php if(isset($_REQUEST['minbudget']) && ( 52000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 52,000,000 </option>
			   <option value="62000000" <?php if(isset($_REQUEST['minbudget']) && ( 62000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 62,000,000 </option>
			   <option value="72000000" <?php if(isset($_REQUEST['minbudget']) && ( 72000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 72,000,000 </option>
			   <option value="82000000" <?php if(isset($_REQUEST['minbudget']) && ( 82000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 82,000,000 </option>
            </select>
          </div>
		   <div id="max-budget" class="property-filter">
            <select name="maxbudget"   class="form-control">
			<option value="0">Max Price</option>
              <option value="12000000"  <?php if(isset($_REQUEST['maxbudget']) && ( 12000000 ==$_REQUEST['maxbudget'])) { echo 'selected;'; }?>>AED 12,000,000</option>
              <option value="22000000" <?php if(isset($_REQUEST['minbudget']) && ( 22000000 ==$_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 22,000,000</option>
              <option value="32000000" <?php if(isset($_REQUEST['minbudget']) && ( 32000000 ==$_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 32,000,000</option>
              <option value="42000000" <?php if(isset($_REQUEST['minbudget']) && ( 42000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 42,000,000</option>
			  <option value="52000000" <?php if(isset($_REQUEST['minbudget']) && ( 52000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 52,000,000</option>
			  <option value="62000000" <?php if(isset($_REQUEST['minbudget']) && ( 62000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 62,000,000</option>
			  <option value="72000000" <?php if(isset($_REQUEST['minbudget']) && ( 72000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 72,000,000</option>
			  <option value="82000000" <?php if(isset($_REQUEST['minbudget']) && ( 82000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 82,000,000</option>
			  <option value="92000000" <?php if(isset($_REQUEST['minbudget']) && ( 92000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 92,000,000</option>
            </select>
          </div>
          <div id="bedroom" class="property-filter">
            <select name="bedroom[]" id="bedroomselect" multiple class="form-control">
             <?php foreach($property_tag as $key =>$value){ ?>
                     <option value="<?php echo $value->term_id; ?>"  <?php if(isset($_REQUEST['bedroom']) && in_array( $value->term_id ,$_REQUEST['bedroom'])) { echo 'selected'; }?> ><?php echo $value->name; ?></option>
            <?php  } ?>
            </select>
          </div>
          <div id="location" class="property-filter">
            <select name="locationnew[]"  id="locationselect" multiple class="form-control">
                <?php foreach($location as $key =>$value){ ?>
                     <option value="<?php echo $value->term_id; ?>"
                             <?php if(isset($_GET['locationnew']) && in_array($value->term_id ,$_GET['locationnew'])) { echo 'selected'; } ?>><?php echo $value->name; ?></option>
            <?php  } ?>
              
            </select>
          </div>
          <div class="search-submit"><button type="submit">Search</button></div>
          <div class="clearfix"></div>
            <input type="hidden" value="Property" name="propertysearch"/>
      
      </div>
    </div>
  </form>
  </div>
</div>