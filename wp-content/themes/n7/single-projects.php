<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header();
$post_meta = get_post_meta(get_the_ID());
$developer =get_the_terms(get_the_ID(),'builder');
$project_type =get_the_terms(get_the_ID(),'project_type');
$property_tag =get_the_terms(get_the_ID(),'property_tag');
$bathroom =get_the_terms(get_the_ID(),'bathroom');

$ourdeveloper = '';
foreach($developer as $key =>$value){
    $ourdeveloper.= $value->name. ',';
    $developer_id = $value->term_id;

}
foreach($project_type as $key =>$value){
    //$projecttype_id.= $value->name. ',';
    $projecttype_id = $value->term_id;

}
$property_tag_key ='';
foreach($property_tag as $key =>$value){
$property_tag_key.= str_replace('BR','',$value->name). ',';

}

$bathroom_key ='';
foreach($bathroom as $key =>$value){
$bathroom_key.= $value->name. ',';

}
$property_tag_key = substr($property_tag_key ,0,-1);
$bathroom_key = substr($bathroom_key ,0,-1);
$ourdeveloper = substr($ourdeveloper ,0,-1);
$pricevariation = '';
$gallery_images = '';
$gallery_thumb ='';
for ($i = 0 ;$i <$post_meta['gallery_images'][0]; $i++)
{
    $thumbimage = wp_get_attachment_image_src($post_meta['gallery_images_'.$i.'_gallery_big_image'][0], $size = 'fullsize');
    $bigimage = wp_get_attachment_image_src($post_meta['gallery_images_'.$i.'_gallery_big_image'][0],$size = 'fullsize');

    $gallerydescription =$post_meta['gallery_images_'.$i.'_image_description'][0];

    $gallery_images .='<div class="item">
                          <img src="'.$bigimage[0].'" alt="'.$gallerydescription.'">
                        </div>';
    $gallery_thumb .='<div class="item"> <img data-original="'.$bigimage[0].'" src="'.$bigimage[0].'" alt="'.$gallerydescription.'"></div>';
}
$no_of_room ='';
$no_of_toilet ='';
    for ($i = 0 ;$i <$post_meta['price_variation'][0]; $i++)
    {
       $no_of_room = $post_meta['price_variation_'.$i.'_no_of_room'][0].',';
       $no_of_toilet = $post_meta['price_variation_'.$i.'_no_of_bathroom'][0].',';
    }



?>

<?php
while ( have_posts() ) :
			the_post(); ?>
<section class="project-details-outer">
    <div class="container">
        <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="about-project-inner">
                <div class="ab-project text">
                    <h2><?php echo get_the_title(); ?></h2>
                    <p><?php echo the_content(); ?></p>
                </div>
                <div class="developer-details text">
                    <h3><?php echo $ourdeveloper; ?> developers</h3>
                    <ul class="if-no-content padding-bottom-10">
                        <?php if (!empty($post_meta['standard_price'][0])){ ?>
                        <li><span><img src="<?php echo get_template_directory_uri(); ?>/images/home-area.png"></span>AED <?php echo number_format($post_meta['standard_price'][0]); ?></li>
                        <?php } else  {?>
										 <li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/home-area.png"></span><span class="rel-des priceonrequest">Price on request</span></li>
									
									<?php } ?>
                        <?php if (!empty($post_meta['area'][0])){ ?>
                        <li><span><img src="<?php echo get_template_directory_uri(); ?>/images/area2.png"></span>AREA: <?php echo $post_meta['area'][0]; ?> SQFT</li>
                        <?php }  ?>
						 <?php if (!empty($property_tag_key)){ ?>
                                            <li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bead-room-grid.png"></span><span class="rel-des"><?php echo $property_tag_key; ?></span>Bedroom Apartment</li>
                                    <?php } ?>
                        <?php if (!empty($bathroom_key)){ ?>
                                            <li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bathroom.png"></span><span class="rel-des"><?php echo $bathroom_key; ?></span>Bathroom</li>
                                   <?php }  ?>
                    </ul>
                </div>
                <div class="developer-details text">
                    <h3>Amenities</h3>
                 
                    <ul class="without-icon if-no-content">
                        <?php  for ($i = 0 ;$i < $post_meta['amenities'][0]; $i++) { ?>
                        <li><?php echo $post_meta['amenities_'.$i.'_amenties_label'][0]; ?></li>
                        <?php } ?>
						 
					
					
                    </ul>
                </div>
                <?php if (!empty($post_meta['possesion'][0])) { ?>
                <div class="explore-dealer"><a href="javascript:;"><?php echo $post_meta['possesion'][0]; ?></a></div>
               <?php }  ?>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="project-inner-gallery">
        <?php if ($gallery_images){ ?>


                    <div class="home-latest-outer">
                      <div id="big" class="owl-carousel owl-theme docs-pictures">
                        <?php echo $gallery_images; ?>

                      </div>
                      <div id="thumbs" class="owl-carousel owl-theme">
                       <?php echo $gallery_images ;?>

                      </div>
                    </div>



        <?php }else{ ?>
                    <div class="home-latest-outer">
                      <div id="big" class="owl-carousel owl-theme">
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>

                      </div>
                      <div id="thumbs" class="owl-carousel owl-theme">
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>
                        <div class="item">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/home-project-plan.jpg">
                        </div>

                      </div>
                    </div>
        <?php } ?>
                 </div>
        </div>
        </div>
    </div>
</section>

<!--end project-details-outer-->

<?php if (!empty($post_meta['pricing_plan'][0])){ ?>
<section class="paymentplan">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-section-heading text">
                <h2>Payment Plan</h2></div>
            </div>
        </div>
	   <div class="row pay_plan">
            <?php if($post_meta['pricing_plan'][0]>0){ for ($i = 0 ;$i < $post_meta['pricing_plan'][0]; $i++ ){ ?>
            <div class="col-md-6">
                <div class="paymentplaninner">
                <h5><?php echo $post_meta['pricing_plan_'.$i.'_price_label'][0] ;?> </h5>
                <p><?php echo $post_meta['pricing_plan_'.$i.'_price_duration_time'][0] ;?> </p>
                </div>
            </div>
			<?php } } ?>
			
	   </div>
    </div>
  
</section>
<?php } ?>
<!--end paymentplan-->
<?php endwhile; ?>

<section class="broker-inquery">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="inner-section-heading text">
<h2>Register your Interest</h2></div>
</div>

<?php include('inc-broker-contact-form.php');?>

</div>
</div>
</section>


<?php include('inc-dealer-contact.php');?>



<section class="related-project">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="inner-section-heading text">
<h2>Similar Properties you might like</h2></div>

<div class="rel-project">
    <div class="owl-carousel owl-theme">
   <?php $args = array('post_type'=>'projects',

            'tax_query' => array(

                    array(
                    'taxonomy' => 'builder',
                    'field' => 'id',
                    'terms' => $developer_id
                    ),
                    array(
                    'taxonomy' => 'project_type',
                    'field' => 'id',
                    'terms' => $projecttype_id
                    ),

                )
             );
        $query=new WP_Query($args);
        while ($query->have_posts() ) :
			$query->the_post();

        $no_of_room ='';
        $no_of_toilet ='';
        for ($i = 0 ;$i <$post_meta['price_variation'][0]; $i++)
        {
            $no_of_room = $post_meta['price_variation_'.$i.'_no_of_room'][0].',';
            $no_of_toilet = $post_meta['price_variation_'.$i.'_no_of_bathroom'][0].',';
        }
         $no_of_toilet = substr($no_of_toilet ,0,-1);
         $no_of_room = substr($no_of_room ,0,-1);
        ?>


        <div class="item">
        <div class="related-project-inner">
        <div class="rel-images"><a href="<?php echo get_the_permalink(); ?>"> <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" ></a></div>
        <div class="rel-description">
        <h4><a href="javascript:;"><?php echo get_the_title(); ?></a></h4>
        <ul>
             <?php if ($no_of_room){ ?>
                    <li class="rel-icon"><span><img src="<?php echo get_template_directory_uri(); ?>/images/bedroom.png"></span><?php echo $no_of_room; ?> Bedroom Apartment</li>
                    <?php } ?>
                    <?php if ($no_of_toilet){ ?>
                    <li class="rel-icon"><span><img src="<?php echo get_template_directory_uri(); ?>/images/bathroom.png"></span><?php echo $no_of_toilet; ?>  Bathroom</li>
                    <?php } ?>
                    <?php if ($post_meta['area'][0]){ ?>
                        <li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/area2.png"></span>AREA: <?php echo $post_meta['area'][0]; ?> SQFT</li>
                        <?php } ?>


        </ul>
        </div>
        </div>
        </div>



    <?php
	endwhile;
?>
   
</div>
</div>

</div>
</div>
</div>
</section>




<?php get_footer(); ?>
