<?php
$terms = get_terms( 'project_type' );
global $wpdb;
$property_type_tarm = get_terms( 'project_property_tag' );
$property_tag = get_terms( array('taxonomy' => 'property_tag') );
$builder = get_terms( array('taxonomy' => 'builder' ) );
$bathroom = get_terms( 'bathroom' );
$location =  get_terms( array('taxonomy' => 'location') );
$term = get_queried_object(); 
if($taxonomy=='project_type')
{
    $innersearch = $term->term_id;
}
if($taxonomy=='project_type')
{
    $innersearch = $term->term_id;
}

if(isset($_REQUEST['innersearch'])  )
{
 wp_reset_query();
        $taxquery = array();
        $metaquery = array();
         if(isset($_REQUEST['developer']))
         {
                 $taxquery[] =array('taxonomy' => 'builder','field' => 'term_id','terms' => $_REQUEST['developer'], 'operator' => 'IN' );
                
         }
    
         if(isset($_REQUEST['property']))
         {
                 $taxquery[] =array('taxonomy' => 'project_property_tag','field' => 'term_id','terms' => $_REQUEST['property'], 'operator' => 'IN' );
                
         }
          if(isset($_REQUEST['bedroom']))
         {
                 $taxquery[] =array('taxonomy' => 'property_tag','field' => 'term_id','terms' => $_REQUEST['bedroom'], 'operator' => 'IN' );
                
         }
         if(isset($_REQUEST['locationnew']))
         {
                 $taxquery[] =array('taxonomy' => 'location','field' => 'term_id','terms' => $_REQUEST['locationnew'], 'operator' => 'IN' );
                
         }
        if(isset($_REQUEST['bathroom']))
         {
                 $taxquery[] =array('taxonomy' => 'bathroom','field' => 'term_id','terms' => $_REQUEST['bathroom'], 'operator' => 'IN' );
                
         }
         if(isset($_REQUEST['active-property']) )
         {
                $taxid = $_REQUEST['active-property'];
                $taxquery[] =array('taxonomy' => 'project_type','field' => 'term_id','terms' => array($taxid), 'operator' => 'IN' );
                
         }
          else if(isset($_REQUEST['innersearch']))
         {
                $taxid =  $_REQUEST['innersearch'];
                $taxquery[] =array('taxonomy' => 'project_type','field' => 'term_id','terms' => array($taxid), 'operator' => 'IN' );
                
         }
       
        if(!empty($_REQUEST['minbudget']) && !empty($_REQUEST['maxbudget']))
        {
           $metaquery[] = array('key'     => 'standard_price',
            'value'   => array( $_REQUEST['minbudget'], $_REQUEST['maxbudget'] ),
            'type'    => 'numeric',
            'compare' => 'BETWEEN');
        }
        if(!empty($_REQUEST['minbudget']) && empty($_REQUEST['maxbudget']))
        {
           $metaquery[] = array('key'     => 'standard_price',
            'value'   =>  $_REQUEST['minbudget'] ,
            'type'    => 'numeric',
            'compare' => '>=');
        }
        if(empty($_REQUEST['minbudget']) && !empty($_REQUEST['maxbudget']))
        {
           $metaquery[] = array('key'     => 'standard_price',
            'value'   =>  $_REQUEST['maxbudget'] ,
            'type'    => 'numeric',
            'compare' => '<=');
        }
       
        
        if(!empty($taxquery))
        {
               $taxquery['relation'] = 'AND';
            
        }
        if(!empty($metaquery))
        {
             $metaquery[] =  $metaquery['relation'] = 'AND';
            
        }
        if(!empty($taxquery))
            $args['tax_query'] = $taxquery;
        if(!empty($metaquery))
            $args['meta_query'] = $metaquery;
    
        $args['post_type'] = 'projects';
        $args['post_status'] = 'publish';
        $args['paged']          = get_query_var( 'paged' );
     if (isset($_SESSION['posts_per_page']))
    {
        // $query->set( 'posts_per_page', $_SESSION['posts_per_page']);
           $args['posts_per_page'] = $_SESSION['posts_per_page'];
    }
           else
           {
                $args['posts_per_page'] = 10;
           }
   
    
    
query_posts($args);
     // echo '<pre>'; print_R( ($args));    echo '</pre>';
   
}

?>



<section class="filter-section inner-filter">
    <div class="container">
      <div class="serach-pannel">
        <form action ="<?php echo get_nopaging_url(); ?>">
        <?php if(isset($_REQUEST['active-property'])){?>
             <input type="hidden" name="innersearch" value="<?php echo $_REQUEST['active-property']; ?>">
        <?php }else  if(isset($_REQUEST['innersearch'])){?>
            <input type="hidden" name="innersearch" value="<?php echo $_REQUEST['innersearch']; ?>">
            <?php }else{    ?>
             <input type="hidden" name="innersearch" value="<?php if(isset($innersearch)) echo $innersearch; ?>">
            <?php } ?>
		  <div class="search-field" id="developer">
             <select name="developer[]" id="developerselect" multiple class="form-control">
              <?php foreach($builder as $key =>$value){ ?>
                     <option value="<?php echo $value->term_id; ?>"
                             <?php if(isset($_GET['developer']) && in_array($value->term_id ,$_GET['developer'])) { echo 'selected'; } ?>><?php echo $value->name; ?></option>
            <?php  } ?>
            </select>
          </div>

         <div id="property-type" class="search-field">
            <select name="property[]" id="property-typeselect" multiple class="form-control">
              <?php foreach($property_type_tarm as $key =>$value){ ?>
                        <option value="<?php echo $value->term_id; ?>"
                             <?php if(isset($_GET['property']) && in_array($value->term_id ,$_GET['property'])) { echo 'selected'; } ?>>
                         <?php echo $value->name; ?>
                        </option>
            <?php  } ?>
            </select>
          </div>
          <div id="budget" class="search-field">
            <select name="minbudget"  id="budgetselect"  class="form-control">
			<option value="0">Min Price</option>
              <option value="200000"  <?php if(isset($_REQUEST['minbudget']) && ( 200000 ==$_REQUEST['minbudget'])) { echo 'selected;'; } ?>>AED 200,000</option>
              <option value="12000000" <?php if(isset($_REQUEST['minbudget']) && ( 12000000 ==$_REQUEST['minbudget'])) { echo 'selected;'; }?>>AED 12,000,000</option>
              <option value="22000000" <?php if(isset($_REQUEST['minbudget']) && ( 22000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 22,000,000</option>
			   <option value="32000000" <?php if(isset($_REQUEST['minbudget']) && ( 32000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 32,000,000</option>
			   <option value="42000000" <?php if(isset($_REQUEST['minbudget']) && ( 42000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 42,000,000 </option>
			   <option value="52000000" <?php if(isset($_REQUEST['minbudget']) && ( 52000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 52,000,000 </option>
			   <option value="62000000" <?php if(isset($_REQUEST['minbudget']) && ( 62000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 62,000,000 </option>
			   <option value="72000000" <?php if(isset($_REQUEST['minbudget']) && ( 72000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 72,000,000 </option>
			   <option value="82000000" <?php if(isset($_REQUEST['minbudget']) && ( 82000000== $_REQUEST['minbudget'])) { echo 'selected'; }?>>AED 82,000,000 </option>
            </select>
          </div>
		   <div id="max-budget" class="search-field">
            <select name="maxbudget"  id="maxbudgetselect" class="form-control">
			<option value="0">Max Price</option>
              <option value="12000000"  <?php if(isset($_REQUEST['maxbudget']) && ( 12000000 ==$_REQUEST['maxbudget'])) { echo 'selected;'; }?>>AED 12,000,000</option>
              <option value="22000000" <?php if(isset($_REQUEST['minbudget']) && ( 22000000 ==$_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 22,000,000</option>
              <option value="32000000" <?php if(isset($_REQUEST['minbudget']) && ( 32000000 ==$_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 32,000,000</option>
              <option value="42000000" <?php if(isset($_REQUEST['minbudget']) && ( 42000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 42,000,000</option>
			  <option value="52000000" <?php if(isset($_REQUEST['minbudget']) && ( 52000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 52,000,000</option>
			  <option value="62000000" <?php if(isset($_REQUEST['minbudget']) && ( 62000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 62,000,000</option>
			  <option value="72000000" <?php if(isset($_REQUEST['minbudget']) && ( 72000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 72,000,000</option>
			  <option value="82000000" <?php if(isset($_REQUEST['minbudget']) && ( 82000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 82,000,000</option>
			  <option value="92000000" <?php if(isset($_REQUEST['minbudget']) && ( 92000000 == $_REQUEST['maxbudget'])) { echo 'selected'; }?>>AED 92,000,000</option>
            </select>
          </div>
          <div id="bedroom" class="search-field">
            <select name="bedroom[]" id="bedroomselect" multiple class="form-control">
             <?php foreach($property_tag as $key =>$value){ ?>
                     <option value="<?php echo $value->term_id; ?>"  <?php if(isset($_REQUEST['bedroom']) && in_array( $value->term_id ,$_REQUEST['bedroom'])) { echo 'selected'; }?> ><?php echo $value->name; ?></option>
            <?php  } ?>
            </select>
          </div>
		   <div id="bathroom" class="search-field">
            <select name="bathroom[]" id="bathroomselect" multiple class="form-control">
              <?php foreach($bathroom as $key =>$value){ ?>
                     <option value="<?php echo $value->term_id; ?>"
                             <?php if(isset($_GET['bathroom']) && in_array($value->term_id ,$_GET['bathroom'])) { echo 'selected'; } ?>><?php echo $value->name; ?></option>
            <?php  } ?>
            </select>
          </div>
          <div id="location" class="search-field">
            <select name="locationnew[]" id="locationselect" multiple class="form-control">
                <?php foreach($location as $key =>$value){ ?>
                    <option value="<?php echo $value->term_id; ?>"
                        <?php if(isset($_GET['locationnew']) && in_array($value->term_id ,$_GET['locationnew'])) { echo 'selected'; } ?>><?php echo $value->name; ?></option>
                <?php  } ?>

            </select>
           
          </div>
          <div class="search-submit search-field"><button type="submit">Search</button></div>
          <div class="clearfix"></div>
        </form>
      </div>
    </div>
  </section>
