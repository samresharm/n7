$(document).ready(function() {
  var bigimageinner = $("#big-inner");
  var thumbsinner = $("#thumbsinner-inner");
  //var totalslides = 10;
  var syncedSecondary = true;

  bigimageinner
    .owlCarousel({
    items: 1,
    slideSpeed: 2000,
    nav: true,
    autoplay: true,
    dots: false,
    loop: true,
    responsiveRefreshRate: 200,
    navText: [
      '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
      '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
    ]
  })
    .on("changed.owl.carousel", syncPosition);

  thumbsinner
    .on("initialized.owl.carousel", function() {
    thumbsinner
      .find(".owl-item")
      .eq(0)
      .addClass("current");
  })
    .owlCarousel({
    items: 4,
    dots: true,
    nav: true,
    navText: [
      '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
      '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
    ],
    smartSpeed: 200,
    slideSpeed: 500,
    slideBy: 4,
    responsiveRefreshRate: 100
  })
    .on("changed.owl.carousel", syncPosition2);

  function syncPosition(el) {
    //if loop is set to false, then you have to uncomment the next line
    //var current = el.item.index;

    //to disable loop, comment this block
    var count = el.item.count - 1;
    var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

    if (current < 0) {
      current = count;
    }
    if (current > count) {
      current = 0;
    }
    //to this
    thumbsinner
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = thumbsinner.find(".owl-item.active").length - 1;
    var start = thumbsinner
    .find(".owl-item.active")
    .first()
    .index();
    var end = thumbsinner
    .find(".owl-item.active")
    .last()
    .index();

    if (current > end) {
      thumbsinner.data("owl.carousel").to(current, 100, true);
    }
    if (current < start) {
      thumbsinner.data("owl.carousel").to(current - onscreen, 100, true);
    }
  }

  function syncPosition2(el) {
    if (syncedSecondary) {
      var number = el.item.index;
      bigimageinner.data("owl.carousel").to(number, 100, true);
    }
  }

  thumbsinner.on("click", ".owl-item", function(e) {
    e.preventDefault();
    var number = $(this).index();
    bigimageinner.data("owl.carousel").to(number, 300, true);
  });
});
