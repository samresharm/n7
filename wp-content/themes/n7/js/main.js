$(function(){
	//var windowwidthmenu = $(window).width();
	$('.trigger').click(function(){
		$('.menu').toggleClass('open');
		$(this).toggleClass('active');
	});
});


$(document).ready(function(){

	var windowwidth1= $(window).width();

	if(windowwidth1>800){
		$(".banner-slider .owl-carousel .item img.main_banner").each(function(i, elem) {
  var img = $(elem);
  var div = $("<div />").css({
    background: "url(" + img.attr("src") + ") no-repeat",
    width: img.width() + "px",
    height: img.height() + "px"
  });
  img.replaceWith(div);
  });
}


function collSec(){
    
 var windowwidth1= $(window).width();
  if(windowwidth1 <= 767){
    //Payment Plans Collapse in mobile
  $(".inner-section-heading h2").on("click", function(){  
   $(".inner-section-heading h2").toggleClass('minus1');  
   $(".pay_plan").slideToggle();  
  });

  //Footer Links Collapse in mobile 
  $(".footer-link h4").on("click", function(){  
    $(this).toggleClass('minus1');
    $(this).next('ul').slideToggle();  
    //$(this).next().slideToggle(); 
  });    
}

}
collSec();


//About page read more 
$(".learn-more.tg-mob a").on("click", function(){
  $('.about-us .homeabout-text p').not(':first').slideToggle();
  $(this).text($(this).text() == 'Read More' ? 'Read Less' : 'Read More');
  //$('.about-us .homeabout-text p:first-child').css('display','block');
});

});



$(document).ready(function(){
	$(window).resize(function(){
			resizeImage();
		});
		resizeImage();
});

	function resizeImage(){

	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var windowHeight1 = ($(window).height());
	$('.banner-slider .owl-carousel .item div:first-child ').css('width' , 'windowWidth');
	$('.banner-slider .owl-carousel .item div:first-child').css('height' , 'windowHeight1');
	$('.banner-slider ').css('height' , 'windowHeight');
	$(window).resize(function(){
		$('.banner-slider .owl-carousel .item div:first-child ').css('width' , 'windowWidth');
		$('.banner-slider .owl-carousel.item div:first-child').css('height' , 'windowHeight1');
		$('.banner-slider').css('height' , 'windowHeight');
	})
	$('.banner-slider .owl-carousel .item div:first-child').width($(window).width());
	$('.banner-slider .owl-carousel .item div:first-child').height( $(window).height());
	$('.banner-slider').height( $(window).height() );
	}




$(function(){
$('.banner-slider .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
	autoplayHoverPause:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
})
})



$(function(){
$('#latest-slider-home .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    dots: true,
    responsiveClass:true,
	  autoplayHoverPause:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:true
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
})
})




$(function(){
$('.home-blog-slider .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
	autoplayHoverPause:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:true
        },
        1000:{
            items:3,
            nav:true,
            loop:true
        }
    }
})
})



$(document).ready(function(){

	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})


$(function(){
$('#home-blog .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
	autoplayHoverPause:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        568:{
            items:1,
            nav:true
        },
         800:{
            items:2,
            nav:true
        },
        1000:{
            items:3,
            nav:true,
            loop:true
        }
    }
})
})



$(function(){
$('.rel-project .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
	autoplayHoverPause:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:true
        },
        1000:{
            items:3,
            nav:true,
            loop:true
        }
    }
})
})



$(function(){
$('.innerbanner-slider .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
	autoplayHoverPause:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:true
        },
        1000:{
            items:1,
            nav:true,
            loop:true
        }
    }
})
})


$(function(){
$('.project-img .owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    responsiveClass:true,
		items:1,
		nav: false,
		dots:true,
		autoplayHoverPause:true
})


/*	$('.innerbanner-slider .owl-carousel').owlCarousel({
	    loop:false,
		autoplay:3000,
			items:1,
			nav: true,
			autoplayHoverPause:true,
			dots:false
	})*/

})

// View an image
$(function () {

  'use strict';

  var console = window.console || { log: function () {} };
  var $images = $('.docs-pictures');
  var $toggles = $('.docs-toggles');
  var $buttons = $('.docs-buttons');
  var options = {
        // inline: true,
        url: 'data-original',
        build: function (e) {
         // console.log(e.type);
        },
        built: function (e) {
          //console.log(e.type);
        },
        show: function (e) {
          //console.log(e.type);
        },
        shown: function (e) {
         // console.log(e.type);
        },
        hide: function (e) {
         // console.log(e.type);
        },
        hidden: function (e) {
         // console.log(e.type);
        },
        view: function (e) {
          //console.log(e.type);
        },
        viewed: function (e) {
         // console.log(e.type);
        }
      };

  function toggleButtons(mode) {
    if (/modal|inline|none/.test(mode)) {
      $buttons.
        find('button[data-enable]').
        prop('disabled', true).
          filter('[data-enable*="' + mode + '"]').
          prop('disabled', false);
    }
  }

  $images.on({
    'build.viewer': function (e) {
      //console.log(e.type);
    },
    'built.viewer':  function (e) {
      //console.log(e.type);
    },
    'show.viewer':  function (e) {
      //console.log(e.type);
    },
    'shown.viewer':  function (e) {
      //console.log(e.type);
    },
    'hide.viewer':  function (e) {
      //console.log(e.type);
    },
    'hidden.viewer': function (e) {
      //console.log(e.type);
    },
    'view.viewer':  function (e) {
      //console.log(e.type);
    },
    'viewed.viewer': function (e) {
      //console.log(e.type);
    }
  }).viewer(options);

  toggleButtons(options.inline ? 'inline' : 'modal');

  $toggles.on('change', 'input', function () {
    var $input = $(this);
    var name = $input.attr('name');

    options[name] = name === 'inline' ? $input.data('value') : $input.prop('checked');
    $images.viewer('destroy').viewer(options);
    toggleButtons(options.inline ? 'inline' : 'modal');
  });

  $buttons.on('click', 'button', function () {
    var data = $(this).data();
    var args = data.arguments || [];

    if (data.method) {
      if (data.target) {
        $images.viewer(data.method, $(data.target).val());
      } else {
        $images.viewer(data.method, args[0], args[1]);
      }

      switch (data.method) {
        case 'scaleX':
        case 'scaleY':
          args[0] = -args[0];
          break;

        case 'destroy':
          toggleButtons('none');
          break;
      }
    }
  });

	$('#list-view').click(function(){
		$(this).hide();
		$('#grid-view').show();
	  $('.buy-grid').toggleClass('buy-list');
	   $('#remove-class').addClass('container');

	});
	$('#grid-view').click(function(){
		$(this).hide();
		$('#list-view').show();
	  $('.buy-grid').toggleClass('buy-list');
	  $('#remove-class').removeClass('container');
		//var listWidth = $('.project-img').width();
		//$('.owl-carousel.owl-loaded').find('.owl-stage').width(listWidth);
		//$('.owl-carousel.owl-loaded').find('.owl-item').width(listWidth);

	});
});

$(function(){
$('.trigger').click(function(){
    $('.mega_menu').toggleClass('open');
    $('.overlay').fadeIn();
    $('.trigger-close').fadeIn();
    $('.trigger').fadeOut();
});

$('.overlay').click(function(){
    $('.mega_menu').removeClass('open');
    $('.overlay, .trigger-close').fadeOut();
    $('.trigger').fadeIn();
})
});



