<?php
/*
* Template Name: rent
*/
get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="project-rent">
	<div class="container">
		<div class="row">
			<div class="col-md-12 p-0">
				<div class="col-md-6 pull-right">
					<div class="rent-inner-banner">
						<img src="<?php echo get_template_directory_uri(); ?>/images/rent-inner-banner.jpg">
					</div>
				</div>
				<div class="col-md-6 pull-left">
					<div class="rent-inner-content">
						<h2>Renting made<br/>
						profitable</h2>
						<p>Say goodbye to the hassles of having to manually list out your property on rent – we here at N7 will help facilitate a higher level of ease in this process by taking care of all these tedious processes. Now, you don’t have to think twice before securing a nice and reliable inflow of funds – everything will be taken care of by us!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="broker-inquery">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner-section-heading text">
				<h2>Register your Interest</h2></div>
			</div>
			<?php include('inc-broker-contact-form.php')?>
		</div>
	</div>
</section>
<!-- <section class="rent-contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="dealer-contact-buttons">
					<h2>Contact US</h2>
					<div class="call-dealer d_req">
						<a href="javascript:;">Call + 971 487 467 67</a>
					</div>
					<div class="whatsaap-dealer d_req">
						<a href="javascript:;">Chat On WhatsApp</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<?php include('inc-dealer-contact.php');?>
<?php endwhile;?>
<?php endif;?>

<?php get_footer(); ?>

</body>
</html>