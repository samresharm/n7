<?php
/*
* Template Name: contact us
*/
get_header(); ?>
<style>
#contact-page{width:100%; max-width:800px; margin:0 auto;}
#contact-page .row{display:block;}
#contact-page .col-md-6{width:100%; max-width:100%;}
.contact-locator{padding:50px 0;}
.contact-locator .section-heading{text-align: left}
</style>
<section class="contact-us privacy-policy-cust">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center text">
				<h2 class="section-heading">have a query? Let’s connect!</h2>
				
				<div id="contact-page">
					<?php include('inc-broker-contact-form.php');?>
				</div>
				
			</div>
		</div>
	</div>
</section>
<section class="contact-locator">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8">
				<div class="contact-location-map">
					<iframe width="100%" height="360" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Westburry Tower 1, Business Bay, Dubai, UAE&amp;key=AIzaSyCZmX3oa7KEnTADxneD3GF_jsW4le8vAn0"></iframe>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="location-add">
					<h2 class="section-heading">find us at</h2>
					<div class="add-block">
						<h3>Dubai Office</h3>
						<p>Office number 1706,<br/> Westburry Tower 1,<br/> Business Bay,<br/> Dubai, UAE,<br/>  P.OBox 376313</p>
					</div>
					<div class="add-block">
						<h3>Australia Office</h3>
						<p>133 Radnor Drive, Deer<br/> Park, Victoria -3023,<br/> Australia</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
</body>
</html>