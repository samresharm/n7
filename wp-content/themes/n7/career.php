<?php
/*
* Template Name: career
*/
get_header();


?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="career-outer">
	<div class="container">
<div class="career">
				<h2 class="text-align-center section-heading">work with us</h2>
				<p>We are hiring new-age professionals to tackle industry challenges, meet customer demand, and pool in informative insights to augment our service offerings. If you feel like you've got what it takes to become a key player in the real estate industry, then what are you waiting for? Apply right now! </p>
		</div>
	</div>
</section>
<section class="career-section2">
	<div class="container">

			<div class="career-inner-content">
				<h2 class="text-align-center section-heading">About working here</h2>
			
			<ul>
			<li><span class="cr_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon1.png"></span><span class="cr_details">Conducive Environment</span></li>
			<li><span class="cr_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon2.png"></span><span class="cr_details">Shared Success</span></li>
			<li><span class="cr_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon3.png"></span><span class="cr_details">Healthy Meals & Snacks</span></li>
			<li><span class="cr_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon4.png"></span><span class="cr_details">Medical Insurance</span></li>
			<li><span class="cr_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon5.png"></span><span class="cr_details">Best Gadgets</span></li>
			<li><span class="cr_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon6.png"></span><span class="cr_details">Culture of Learning</span></li>
			</ul>
			
			
			
			</div>

	</div>
</section>

<section class="career-collection">
<div class="career-images"><img src="<?php echo get_template_directory_uri(); ?>/images/career1.jpg"></div>
<div class="career-images"><img src="<?php echo get_template_directory_uri(); ?>/images/career2.jpg"></div>
<div class="career-images"><img src="<?php echo get_template_directory_uri(); ?>/images/career3.jpg"></div>
<div class="clearfix"></div>
</section>



<section class="open-position">
<h2 class="text-align-center section-heading">Open Positions</h2>

<div class="open-position-inner">
<ul class="tabs">
<li class="tab-link current" data-tab="tab-1">Marketing</li>
<li class="tab-link" data-tab="tab-2">Sales</li>
<li class="tab-link" data-tab="tab-3">Human Resources</li>
<li class="tab-link" data-tab="tab-4">Others</li>
</ul>


<div id="tab-1" class="tab-content current">
<div class="job_inner">
<div class="job-left"><h3>Marketing Manager</h3>
<p>7-8 Years of relevant experience </p>
</div>
<div class="job-right"><div class="apply-now"><a href="mailto:">APPLY NOW</a></div></div>
</div>


<div class="job_inner">
<div class="job-left"><h3>Part-time Marketing Executive</h3>
<p>5-7 years of relevant experience </p>
</div>
<div class="job-right"><div class="apply-now"><a href="mailto:">APPLY NOW</a></div></div>
</div>



<div class="job_inner">
<div class="job-left"><h3>Market Research Analyst</h3>
</div>
<div class="job-right"><div class="apply-now"><a href="mailto:">APPLY NOW</a></div></div>
</div>

</div>
<div id="tab-2" class="tab-content">

</div>
<div id="tab-3" class="tab-content"></div>
<div id="tab-4" class="tab-content"></div>

</div>


</section>


<?php endwhile;?>
<?php endif;?>

<?php get_footer(); ?>
</body>
</html>