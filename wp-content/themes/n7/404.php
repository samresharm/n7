<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<section class="page-not-found">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3 class="section-heading">Page Not Found</h3>
				<div class="learn-more"><a href="<?php echo get_site_url();?>">Go to Home</a></div>
			</div>
		</div>
	</div>
	
</section>


<?php get_footer(); ?>
