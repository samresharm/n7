<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;



get_header(); ?>


<?php if ( have_posts() ) : ?>

<?php include('inner-search.php');?>

<section class="pagination-sec">
  <div class="container">
    <div class="d-flex justify-content-md-end flex-wrap justify-content-sm-center">
      <div class="view-div">
        <ul>
          <li><a href="javascript:;"><i class="fa fa-list-ul" aria-hidden="true"></i></a></li>
        </ul>
      </div>

      <div class="pagination-div">
          <?php echo wpbeginner_numeric_posts_nav() ;?>

      </div>
      <div class="perpage-view">
        <div class="select">
        <form name="myform" action="" method="post">
           
              <select name='posts_per_page' onchange="myform.submit();">
              <option value='10' <?php if(isset($_SESSION['posts_per_page']) && $_SESSION['posts_per_page'] ==10 ) echo 'selected'; ?>> 10 per page </option>
              <option value='20' <?php if(isset($_SESSION['posts_per_page']) && $_SESSION['posts_per_page'] ==20 ) echo 'selected'; ?>> 20 per page </option>
              <option value='30' <?php if(isset($_SESSION['posts_per_page']) && $_SESSION['posts_per_page'] ==30 ) echo 'selected'; ?>> 30 per page </option>
              <option value='40' <?php if(isset($_SESSION['posts_per_page']) && $_SESSION['posts_per_page'] ==40 ) echo 'selected'; ?>> 40 per page </option>
              <option value='50' <?php if(isset($_SESSION['posts_per_page']) && $_SESSION['posts_per_page'] ==50 ) echo 'selected'; ?>> 50 per page </option>
            </select>
            
        </form>

      </div>
      </div>
    </div>
  </div>
</section>
<section class="plisting-sec" id="off-plan">
  <div>

			<?php
			// Start the Loop.
            $k =0;
			while ( have_posts() ) :
				the_post();
                    $post_meta = get_post_meta(get_the_ID());
                    $developer =get_the_terms(get_the_ID(),'builder');
					$location =get_the_terms(get_the_ID(),'location');
					$property_tag =get_the_terms(get_the_ID(),'property_tag');
					$bathroom =get_the_terms(get_the_ID(),'bathroom'); 

                    $ourdeveloper = '';
                    foreach($developer as $key =>$value){
                    $ourdeveloper.= $value->name. ',';

                    }
					$locationkey ='';
					foreach($location as $key =>$value){
                    $locationkey.= $value->name. ',';

                    }
					
					$bathroomkey ='';
					foreach($bathroom as $key =>$value){
                    $bathroomkey.= $value->name. ',';

                    }
					
					$locationkey = substr($locationkey ,0,-1);
					
					$property_tag_key ='';
					foreach($property_tag as $key =>$value){
                    $property_tag_key.= str_replace('BR','',$value->name). ',';

                    }
					$property_tag_key = substr($property_tag_key ,0,-1);
                    $no_of_room ='';
                    $no_of_toilet ='';
                    for ($i = 0 ;$i <$post_meta['price_variation'][0]; $i++)
                    {
                        //$no_of_room = $post_meta['price_variation_'.$i.'_no_of_room'][0].',';
                        $no_of_toilet = $post_meta['price_variation_'.$i.'_no_of_bathroom'][0].',';
                    }
                    $ourdeveloper = substr($ourdeveloper ,0,-1);
                    $pricevariation = '';
                    $gallery_images = '';
                    $gallery_thumb ='';
                    for ($i = 0 ;$i <$post_meta['gallery_images'][0]; $i++)
                    {
						if($i>2) break;
                        $thumbimage = wp_get_attachment_image_src($post_meta['gallery_images_'.$i.'_gallery_big_image'][0],'full size');
                        $bigimage = wp_get_attachment_image_src($post_meta['gallery_images_'.$i.'_gallery_big_image'][0],'full size');

                        $gallerydescription =$post_meta['gallery_images_'.$i.'_image_description'][0];

                        $gallery_images .='<div class="item"><figure><img data-original="'.$bigimage[0].'" src="'.$bigimage[0].'" alt="'.$gallerydescription.'"></figure></div>';

                    }




            ?>
					<div class="c_show">
                    <div class="project-list main-height">

                            <div class="project-img eq-height">
                                <div class="owl-carousel owl-theme docs-pictures">

                                   <?php if($gallery_images){ echo $gallery_images; }
								   else{ ?>
								   <div class="item"><figure><img data-original="<?php echo get_template_directory_uri(); ?>/images/gallery-dummy.jpg" src="<?php echo get_template_directory_uri(); ?>/images/gallery-dummy.jpg" alt=""></figure></div>
								   <?php } ?>
                                </div>
                            </div>
                            <div style="display:none" class="feature-img">
                                <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" >
                            </div>


                            <div class="project-discription eq-height">
							<div class="p_table">
							<div class="p_cell">
                                <div class="project-name">
                                   <p class="dev_loper"><?php echo $ourdeveloper; ?></p>
                                    <h3><a href="<?php echo get_permalink();?>"><?php echo get_the_title(); ?></a></h3>
                                </div>
                                <div class="rel-description">
                                    <p><?php echo get_the_excerpt(); ?>...</p>
                                    <ul>
									
									<?php if (!empty($locationkey)){ ?>
                                            <li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/location-icon.png"></span><span class="rel-des"><?php echo $locationkey; ?></span></li>
                                     <?php } else { ?>
									<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/location-icon.png"></span><span class="rel-des">Coming soon</span></li>
									
									 <?php } ?>
									
									<?php if (!empty($property_tag_key)){ ?>
                                            <li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bead-room-grid.png"></span><span class="rel-des"><?php echo $property_tag_key; ?></span>Bedroom Apartment</li>
                                     <?php } ?>
									
									 <?php if ($post_meta['standard_price'][0]){ ?>
                                            <li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/home-iconb.png"></span><span class="rel-des">AED <?php echo number_format($post_meta['standard_price'][0]); ?></span></li>
                                    <?php } else{ ?>
									<li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/home-iconb.png"></span><span class="rel-des priceonrequest">Price on request </span></li>
									<?php } ?>
                                    </ul>
                                </div>
                                <div class="dealer-contact-buttons">
                                    <h2>Contact Us</h2>
                                    <div class="call-dealer d_req">
                                        <a href="tel:+97148746767">Call + 971 487 467 67</a>
                                    </div>
                                    <div class="whatsaap-dealer d_req">
                                        <a href="https://wa.me/97148746767">Chat On WhatsApp</a>
                                    </div>
                                </div>
								</div>
								</div>
								<!--end eq-height-->
                            </div>
                      <div class="clearfix"></div>
                    </div>
					</div>
      <?php
			endwhile;

			// Previous/next page navigation.

?>
    </div>
</section>
<div class="container">
  <div class="row">
    <div class="col-md-6 ">
    <div class="laodmore-btn" style="text-align: right">
      <a href="javascript:;" id="loadMore">View more</a>
    </div>
  </div>
  <div class="col-md-6">
    <div class="pagination-div bottom-right">
        <?php echo wpbeginner_numeric_posts_nav() ;?>
    </div>
  </div>
  </div>
</div>


<section class="related-project">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="inner-section-heading text">
<h2>Similar Properties you might like</h2></div>

<div class="rel-project">
    <div class="owl-carousel owl-theme">
   <?php $args = array('post_type'=>'projects',

            'tax_query' => array(

                    array(
                    'taxonomy' => 'builder',
                    'field' => 'id',
                    'terms' => '48'
                    ),
                    array(
                    'taxonomy' => 'project_type',
                    'field' => 'id',
                    'terms' => '44'
                    ),

                )
             );
        $query=new WP_Query($args);
        while ($query->have_posts() ) :
			$query->the_post();

        $no_of_room ='';
        $no_of_toilet ='';
        for ($i = 0 ;$i <$post_meta['price_variation'][0]; $i++)
        {
            $no_of_room = $post_meta['price_variation_'.$i.'_no_of_room'][0].',';
            $no_of_toilet = $post_meta['price_variation_'.$i.'_no_of_bathroom'][0].',';
        }
        $no_of_toilet = substr($no_of_toilet ,0,-1);
        ?>


        <div class="item">
        <div class="related-project-inner">
        <div class="rel-images"><a href="<?php echo get_the_permalink(); ?>"> <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" ></a></div>
        <div class="rel-description">
        <h4><a href="<?php echo get_permalink();?>"><?php echo get_the_title(); ?></a></h4>
        <ul>
             <?php if ($no_of_room){ ?>
                    <li class="rel-icon"><span><img src="<?php echo get_template_directory_uri(); ?>/images/bedroom.png"></span><?php echo $no_of_room; ?> Bedroom Apartment</li>
                    <?php } ?>
                    <?php if ($no_of_toilet){ ?>
                    <li class="rel-icon"><span><img src="<?php echo get_template_directory_uri(); ?>/images/bathroom.png"></span><?php echo $no_of_toilet; ?>  Bathroom</li>
                    <?php } ?>
                    <?php if ($post_meta['area'][0]){ ?>
                        <li><span class="rel-icon"><img src="<?php echo get_template_directory_uri(); ?>/images/area2.png"></span>AREA: <?php echo $post_meta['area'][0]; ?> SQFT</li>
                        <?php } ?>


        </ul>
        </div>
        </div>
        </div>



    <?php
	endwhile;
?>

</div>
</div>

</div>
</div>
</div>
</section>







<?php
			// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>


<?php get_footer(); ?>
