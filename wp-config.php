<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'n7' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Mysql@1234$#$' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Hx-I{r+e*tOo|KRkf$_br`}0BUP]KkJi>iOBu6DOB!WV3M{w9B=cJ5S3Cygn$w|%' );
define( 'SECURE_AUTH_KEY',  'C[Tm.1O0InmUJWkrIjHLS)a8Z@so.I{1UE|9xrX;]lTTT2iJBoS-Sg~1Wpg6u0MP' );
define( 'LOGGED_IN_KEY',    'OHVcZ0Wbq%}2mrL`,z4>YyV4I+;4kyS(i=z($Ey5P1P[a0SS5E6&yUteT6|Wan8*' );
define( 'NONCE_KEY',        '](|!ZqX79?b5o;Hrx^dsZdN)=]S*Z4^Q#_>-dbL-e}q,}8`TpI4Ktf#oTS|)Jz 1' );
define( 'AUTH_SALT',        'vyIpTKA:ZyNqUXA[bn0*-;[}`{_rkV{%ZH8]u2ts^/)/V&x:xrBHN|!~SNa7[L$e' );
define( 'SECURE_AUTH_SALT', '3mXLd!9H-(.nI2LB.s:,lEwk42N~f&21^1ucR WmUz(P+U5ESG$yG?UWMNH=zIn[' );
define( 'LOGGED_IN_SALT',   '>4oaM|4FMZGeL.M2g=:!c]yj5l89^Wo&%z4c$;%%c#_CXIY20VZ>d%pVcvC+pJf,' );
define( 'NONCE_SALT',       '.f47&EX9>&$%[ui(3$W%4ua]mCXj=nhz!Yivv<3aruSs1Ui+sQ]`),BT=|H<9L-Q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'n7_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
